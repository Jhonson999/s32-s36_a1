const Course = require("../models/Course");


module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
		

	})

	return newCourse.save().then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
//Retrieve all the courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

//Retrieve all active courses
module.exports.getallActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retrieve a specific courses

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};

//Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	//Syntax:
	    //findByIdAndUpdate(document, updatesToBeApplied)

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//2 Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: false
	}



	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}