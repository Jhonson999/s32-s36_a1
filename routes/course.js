const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")



router.post("/", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
		if (isAdmin){
			res.send('Admin verified. Course created successfully.')
			courseController.addCourse(req.body)
		}
		else{
			res.send('Admin not verified. Course was not created successfully.')
		}
})

//Retrieve all the courses
router.get("/all",(req, res) => {
	courseController.getAllCourses().then(resultFromController =>
		res.send(resultFromController))
});

//Retrieve all active course

router.get("/", (req, res) => {
	courseController.getallActive().then(resultFromController =>
		res.send(resultFromController))
});

//Retrieve specific course

router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController =>
		res.send(resultFromController))
});

//Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});
//1 Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
//3 Process a PUT request at the /courseID/archive route using postman to archive a course
//Archive a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(
		resultFromController => res.send(resultFromController))
});


module.exports = router;