	const express = require("express");
	const router = express.Router();
	const userController = require("../controllers/user");
	const auth = require("../auth");


	router.post("/checkEmail", (req, res) =>{
		userController.checkEmailExists(req.body).then(resultFromController =>
			res.send(resultFromController))
	})

	router.post("/register", (req, res) => {
		userController.registerUser(req.body).then(resultFromController =>
			res.send(resultFromController))
	})

	router.post("/login", (req, res) =>{
		userController.loginUser(req.body).then(resultFromController =>
			res.send(resultFromController));
	})

	//1. Create a details route that will accept the user's Id to retrieve the details of a user.
	router.get("/details", auth.verify, (req, res) => {
		const userData = auth.decode(req.headers.authorization)
	//2. Create a getProfile controller method for retrieving the details of the user:
	   //2.a. Find the document in the database using the user's ID
		userController.getProfile({userId:req.body.id}).then(resultFromController =>
			res.send(resultFromController));
	})

	//For enrolling a user
	router.post("/enroll",(req, res) => {

		let data = {
			userId: req.body.userId,
			courseId: req.body.courseId
		}

		userController.enroll(data).then(resultFromController =>
			res.send(resultFromController));
	})

	//For enrolling a course
	router.post("/enroll", auth.verify, (req, res) => {
	    let userData = auth.decode(req.headers.authorization)

	let courseData = {
	userId: userData.id,
	courseId: req.body.courseId
	}

	   userController.enroll(courseData).then(resultFromController =>
			res.send(resultFromController));
	})

	

	module.exports = router;